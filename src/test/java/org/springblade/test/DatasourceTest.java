package org.springblade.test;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springblade.core.mp.support.Condition;
import org.springblade.core.test.BladeBootTest;
import org.springblade.core.test.BladeSpringRunner;
import org.springblade.core.tool.utils.Func;
import org.springblade.modules.desk.service.INoticeService;
import org.springblade.modules.develop.dynamic.DataSourceBean;
import org.springblade.modules.develop.dynamic.DataSourceContext;
import org.springblade.modules.develop.entity.Datasource;
import org.springblade.modules.develop.entity.TableInfo;
import org.springblade.modules.develop.mapper.ISqlMapper;
import org.springblade.modules.develop.mapper.MyBatisMapper;
import org.springblade.modules.develop.service.IDatasourceService;
import org.springblade.modules.develop.service.IMyBatisService;
import org.springblade.modules.develop.service.impl.DatasourceServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;

import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@RunWith(BladeSpringRunner.class)
@BladeBootTest(appName = "blade-runner", profile = "test")
public class DatasourceTest {
	@Autowired
	IDatasourceService datasourceService;
	@Autowired
	ISqlMapper iSqlMapper;
	@Autowired
	private IMyBatisService myBatisService;
	@Autowired
	private MyBatisMapper myBatisMapper;

	@Test
	public void testDynamicDataSource() {
		//1.从表中获取需要切换的数据源信息
		List<Datasource> dataSourceDOList = datasourceService.list(Wrappers.<Datasource>lambdaQuery().eq(Datasource::getId, 4));
		//2.循环遍历,切换数据源,执行操作,切完就得切回默认数据源
		for (Datasource dataSourceDO : dataSourceDOList) {
			DataSourceBean.DataSourceBeanBuilder dsbb=new DataSourceBean.DataSourceBeanBuilder(dataSourceDO.getDatabaseName(),
				dataSourceDO.getDatabaseIp(), dataSourceDO.getDatabasePort(), dataSourceDO.getDatabaseName(),
				dataSourceDO.getUsername(), dataSourceDO.getPassword());
			dsbb.driverClassName(dataSourceDO.getDriverClass());
			dsbb.setUrl(dataSourceDO.getUrl());
			DataSourceBean dataSourceBean = new DataSourceBean(dsbb);
			System.out.println(ToStringBuilder.reflectionToString(dataSourceBean));
			DataSourceContext.setDataSource(dataSourceBean);
			List<Map<String, Object>> list=iSqlMapper.selectList("select * from t_everyday_gupiao");
			System.out.println(ToStringBuilder.reflectionToString(list));
			//XXX你的操作XXX
			DataSourceContext.toDefault();
		}
	}

	@Test
	public void test1(){
		List<TableInfo> lis=myBatisMapper.selectTables("gupiao",null, null);
		System.out.println(ToStringBuilder.reflectionToString(lis.get(0)));
		Page page = new Page(0, 10L);
		IPage<TableInfo> list=myBatisService.selectTables(page,"gupiao",new ArrayList<>());
		System.out.println(ToStringBuilder.reflectionToString(list.getRecords().get(0)));
	}
}
