package org.springblade.test.csv;

import org.apache.commons.lang.time.DateFormatUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springblade.core.test.BladeBootTest;
import org.springblade.core.test.BladeSpringRunner;
import org.springblade.modules.develop.mapper.ISqlMapper;
import org.springblade.modules.develop.utils.MybatisUtils;

import java.util.Date;
import java.util.List;
import java.util.Map;

@RunWith(BladeSpringRunner.class)
@BladeBootTest(appName = "csv", profile = "test")
public class CsvTest {
	/**
	 * 下载地址：
	 * https://www.ricequant.com/research/user/user_386135/files/learn/ys/meirishaixuan/2020-01-08.csv?download=1
	 * https://www.ricequant.com/research/user/user_386135/files/learn/ys/meirishaixuan/junxian/2020-01-08.csv?download=1
	 * https://www.ricequant.com/research/user/user_386135/files/learn/ys/meirishaixuan/xinhao/2020-01-08.csv?download=1
	 * https://www.ricequant.com/research/user/user_386135/files/learn/ys/meirishaixuan/zhongdianguanzhu/2020-01-08.csv?download=1
	 */

	@Test
	public void test1() throws Exception {
		ISqlMapper sqlMapper = MybatisUtils.getMapper(ISqlMapper.class);
		String filePath = "E:\\logs\\2020-01-10(3).csv";
		Map<String,Object> csvMap=CSVUtil.readCSV(filePath);
		List<String> headList= (List<String>) csvMap.get("head");
		List<Map<String,String>> dataList= (List<Map<String, String>>) csvMap.get("dataList");
		String startDate="2020-01-10";
		for(Map<String,String> data:dataList){
			System.out.println(data.get("order_book_id"));
			System.out.println(data.get("symbol"));
			String sql="INSERT INTO `my_gp` ( `order_book_id`, `symbol`, `start_date`, `create_time`, `new_close`) VALUES ('"+data.get("order_book_id")+"', '"+data.get("symbol")+"', '"+startDate+"', now(), "+data.get("new_close")+");";
			sqlMapper.insert(sql);
			Thread.sleep(100L);
		}
		MybatisUtils.getSession().commit();
	}

	@Test
	public void test2(){
		ISqlMapper sqlMapper = MybatisUtils.getMapper(ISqlMapper.class);
		String date="2020-01-05";
		String sql="select order_book_id,symbol,count(1) from `my_gp` where start_date>'"+date+"' and start_date<now() group by order_book_id order by count(1) desc";
		List<Map<String, Object>> list=sqlMapper.selectList(sql);
		System.out.println(list);
	}
}
