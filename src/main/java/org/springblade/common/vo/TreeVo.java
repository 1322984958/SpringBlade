package org.springblade.common.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TreeVo {
	private String label;
	private String value;
	private List<TreeVo> children;
}
