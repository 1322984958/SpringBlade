package org.springblade.modules.develop.entity;

import org.apache.commons.lang.StringUtils;
import org.springblade.core.mp.base.BaseEntity;

public class ColumnInfo extends BaseEntity {
	private String columnName;
	private String ordinalPosition;
	private String dataType;
	private String columnType;
	private String characterMaximumLength;
	private String characterOctetLength;
	private String numericPrecision;
	private String numericScale;
	private String columnComment;

	public String getColumnName() {
		return columnName;
	}

	public void setColumnName(String columnName) {
		this.columnName = columnName;
	}

	public String getOrdinalPosition() {
		return ordinalPosition;
	}

	public void setOrdinalPosition(String ordinalPosition) {
		this.ordinalPosition = ordinalPosition;
	}

	public String getDataType() {
		return dataType;
	}

	public void setDataType(String dataType) {
		this.dataType = dataType;
	}

	public String getCharacterMaximumLength() {
		return characterMaximumLength;
	}

	public void setCharacterMaximumLength(String characterMaximumLength) {
		this.characterMaximumLength = characterMaximumLength;
	}

	public String getCharacterOctetLength() {
		return characterOctetLength;
	}

	public void setCharacterOctetLength(String characterOctetLength) {
		this.characterOctetLength = characterOctetLength;
	}

	public String getNumericPrecision() {
		return numericPrecision;
	}

	public void setNumericPrecision(String numericPrecision) {
		this.numericPrecision = numericPrecision;
	}

	public String getNumericScale() {
		return numericScale;
	}

	public void setNumericScale(String numericScale) {
		this.numericScale = numericScale;
	}

	public String getColumnComment() {
		return columnComment;
	}

	public void setColumnComment(String columnComment) {
		this.columnComment = columnComment;
	}
	
	public String getLength(){
		if(StringUtils.isNotBlank(characterMaximumLength)){
			return characterMaximumLength;
		}else if(StringUtils.isNotBlank(characterOctetLength)){
			return characterOctetLength;
		}else if(StringUtils.isNotBlank(columnType)){
			return StringUtils.substring(columnType,columnType.indexOf('(')+1,columnType.indexOf(')'));
		}{
			return StringUtils.defaultString(numericPrecision, "0")+"、"+StringUtils.defaultString(numericScale, "0");
		}
	}

	public String getColumnType() {
		return columnType;
	}

	public void setColumnType(String columnType) {
		this.columnType = columnType;
	}
}
