package org.springblade.modules.develop.entity;

import com.baomidou.mybatisplus.annotation.TableField;

import java.util.List;

public class TableInfo {
	private String tableName;
	private String tableComment;
	private String tableSchema;
	@TableField(exist = false)
	private List<ColumnInfo> columnInfoList;

	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	public String getTableComment() {
		return tableComment;
	}

	public void setTableComment(String tableComment) {
		this.tableComment = tableComment;
	}

	public String getTableSchema() {
		return tableSchema;
	}

	public void setTableSchema(String tableSchema) {
		this.tableSchema = tableSchema;
	}

	public List<ColumnInfo> getColumnInfoList() {
		return columnInfoList;
	}

	public void setColumnInfoList(List<ColumnInfo> columnInfoList) {
		this.columnInfoList = columnInfoList;
	}

}
