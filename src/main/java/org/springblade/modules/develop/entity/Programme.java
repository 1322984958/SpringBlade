/**
 * Copyright (c) 2018-2028, Chill Zhuang 庄骞 (smallchill@163.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.springblade.modules.develop.entity;

import java.io.Serializable;
import java.time.LocalDateTime;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.Version;
import com.fasterxml.jackson.annotation.JsonIgnore;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 项目参数表实体类
 *
 * @author Blade
 * @since 2019-06-24
 */
@Data
@ApiModel(value = "Programme对象", description = "项目参数表")
public class Programme implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * id
	 */
	@ApiModelProperty(value = "id")
	@TableId(value = "id", type = IdType.AUTO)
	private Long id;
	
	@ApiModelProperty(value = "版本号")
	@JsonIgnore
	@Version
	private Long version;
	/**
	 * 项目中文名
	 */
	@ApiModelProperty(value = "项目中文名")
	private String name;
	/**
	 * 数据库名
	 */
	@ApiModelProperty(value = "数据库名")
	private String databaseName;
	/**
	 * 变量名
	 */
	@ApiModelProperty(value = "变量名")
	private String varName;
	/**
	 * 前端项目路径
	 */
	@ApiModelProperty(value = "前端项目路径")
	private String frontProjectPath;
	/**
	 * java项目路径
	 */
	@ApiModelProperty(value = "java项目路径")
	private String javaProjectPath;
	@ApiModelProperty(value = "方法代码路径")
	private String methodCodePath;
	/**
	 * 状态:1.初始化,2.已删除
	 */
	@ApiModelProperty(value = "状态:1.初始化,2.已删除")
	private Integer state;
	/**
	 * 创建时间
	 */
	@ApiModelProperty(value = "创建时间")
	private LocalDateTime createTime;
	private String description;
	/**
	 * 排序值
	 */
	@ApiModelProperty(value = "排序值")
	private Integer sortNum;

}
