package org.springblade.modules.develop.mapper;

import java.util.List;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import org.springblade.modules.develop.entity.ColumnInfo;
import org.springblade.modules.develop.entity.TableInfo;

public interface MyBatisMapper extends BaseMapper<ColumnInfo> {
	@Select("select schema_name from information_schema.SCHEMATA;")
	List<String> selectDatabases();

	@Select("<script>select table_name,table_comment,table_schema from information_schema.tables where table_schema=#{tableSchema} and table_type='base table'" +
		"<if test='tableNamesList!=null and tableNamesList.size()>0'> and table_name in" +
		"<foreach collection='tableNamesList' open='(' item='item' separator=',' close=')'> #{item}</foreach></if>;</script>")
	List<TableInfo> selectTables(@Param("tableSchema") String tableSchema, @Param("tableNamesList") List<String> tableNamesList,IPage page);
	
	@Select("select COLUMN_NAME,ORDINAL_POSITION,DATA_TYPE,COLUMN_TYPE,CHARACTER_MAXIMUM_LENGTH,CHARACTER_OCTET_LENGTH,NUMERIC_PRECISION,NUMERIC_SCALE,COLUMN_COMMENT from information_schema.COLUMNS where table_schema=#{tableSchema} and table_name = #{tableName} order by ORDINAL_POSITION")
	List<ColumnInfo> selectColumnInfo(@Param("tableSchema") String tableSchema, @Param("tableName") String tableName);
	@Select("select COLUMN_NAME,ORDINAL_POSITION,DATA_TYPE,CHARACTER_MAXIMUM_LENGTH,CHARACTER_OCTET_LENGTH,NUMERIC_PRECISION,NUMERIC_SCALE,COLUMN_COMMENT from information_schema.COLUMNS where table_schema=#{tableSchema} and table_name = #{tableName} and COLUMN_NAME=#{columnName} order by ORDINAL_POSITION limit 0,1")
	ColumnInfo getColumnInfo(@Param("tableSchema") String tableSchema, @Param("tableName") String tableName, @Param("columnName") String columnName);
}
