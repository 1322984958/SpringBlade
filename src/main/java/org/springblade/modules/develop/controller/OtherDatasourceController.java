package org.springblade.modules.develop.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springblade.core.boot.ctrl.BladeController;
import org.springblade.core.launch.constant.AppConstant;
import org.springblade.core.mp.support.Condition;
import org.springblade.core.mp.support.Query;
import org.springblade.core.tool.api.R;
import org.springblade.core.tool.utils.Func;
import org.springblade.modules.develop.entity.ColumnInfo;
import org.springblade.modules.develop.entity.Datasource;
import org.springblade.modules.develop.entity.TableInfo;
import org.springblade.modules.develop.service.IMyBatisService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@AllArgsConstructor
@RequestMapping("/otherDatasource/")
@Api(value = "其他数据源", tags = "其他数据源接口")
public class OtherDatasourceController extends BladeController{
	IMyBatisService iMyBatisService;

	/**
	 * 分页 数据源配置表
	 */
	@GetMapping("/list")
	@ApiOperationSupport(order = 2)
	@ApiOperation(value = "分页", notes = "传入datasource")
	public R list() {
		List<String> pages = iMyBatisService.selectDatabases();
		return R.data(pages);
	}
	/**
	 * 分页 数据源配置表
	 */
	@GetMapping("/selectTables")
	@ApiOperationSupport(order = 2)
	@ApiOperation(value = "分页", notes = "查看数据库表")
	public R selectTables(@RequestParam String tableSchema, String tableNamesList, Query query) {
		IPage<TableInfo> pages = iMyBatisService.selectTables(Condition.getPage(query),tableSchema, Func.toStrList(tableNamesList));
		return R.data(pages);
	}


	/**
	 * 分页 数据源配置表
	 */
	@GetMapping("/fieldList")
	@ApiOperationSupport(order = 2)
	@ApiOperation(value = "分页", notes = "查看数据库表")
	public R fieldList(@RequestParam String tableSchema, String tableName, Query query) {
		List<ColumnInfo> pages = iMyBatisService.selectColumnInfo(tableSchema,tableName);
		return R.data(pages);
	}
}
