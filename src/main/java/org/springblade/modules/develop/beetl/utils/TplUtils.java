package org.springblade.modules.develop.beetl.utils;

import org.apache.commons.lang.StringUtils;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

public class TplUtils {
	public static String getTplPathByType(String type){
		String result="business/"+type+".txt";
		return result;
	}
	
	public static JSONObject findJsonObjectByKey(Object o,String key){
		if(o==null|| StringUtils.isBlank(key)){
			return null;
		}
		if(o instanceof JSONObject){
			JSONObject jSONObject=(JSONObject)o;
			if(key.equals(jSONObject.getString("key"))){
				return jSONObject;
			}else{
				JSONArray jSONArray=jSONObject.getJSONArray("children");
				return findJsonObjectByKey(jSONArray, key);
			}
		}else if(o instanceof JSONArray){
			JSONArray jSONArray=(JSONArray)o;
			for(int i=0;i<jSONArray.size();i++){
				JSONObject jSONObject=jSONArray.getJSONObject(i);
				return findJsonObjectByKey(jSONObject, key);
			}
		}
		return null;
	}

	public static void main(String[] args) {
		String newDeviceCode = StringUtils.leftPad(Integer.toHexString("33FFD905504232372784224300000000".hashCode()),12,"0");
		System.out.println(newDeviceCode);
	}
}
