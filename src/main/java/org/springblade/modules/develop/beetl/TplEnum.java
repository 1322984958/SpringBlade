package org.springblade.modules.develop.beetl;

import lombok.Data;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.builder.ToStringBuilder;

import java.io.File;
import java.util.Arrays;

public enum  TplEnum {
	avueCrudEntity("avueCrud","avue的crud组件","entity","entity","javaNew.txt","java","entity","",".java"),
	avueCrudMapper("avueCrud","avue的crud组件","mapper","mapper","mapper.txt","java","mapper","","Mapper.java"),
	avueCrudService("avueCrud","avue的crud组件","service","service","service.txt","java","service","","Service.java"),
	avueCrudServiceImpl("avueCrud","avue的crud组件","serviceImpl","serviceImpl","serviceImpl.txt","java","service"+File.separator+"impl","","ServiceImpl.java"),
	avueCrudVO("avueCrud","avue的crud组件","vo","vo","vo.txt","java","vo","","VO.java"),
	avueCrudWrapper("avueCrud","avue的crud组件","wrapper","wrapper","wrapper.txt","java","wrapper","","Wrapper.java"),
	avueCrudController("avueCrud","avue的crud组件","controller","controller","controller.txt","java","controller","","Controller.java"),
	avueCrudView("avueCrud","avue的crud组件","crudView","crudView","crudView.txt","vue","","views",".vue"),
	avueCrudApi("avueCrud","avue的crud组件","crudApi","crudApi","crudApi.txt","vue","","api",".js"),
	businessRelationVO("businessRelation","业务关联","vo","vo","vo.txt","java","vo","","VO.java"),
	businessRelationWrapper("businessRelation","业务关联","wrapper","wrapper","wrapper.txt","java","wrapper","","Wrapper.java"),
	businessRelationCascader("businessRelation","业务关联","cascader","cascader","cascader.txt","vue","","views","Cascader.vue"),
	businessRelationSelect("businessRelation","业务关联","cascaderSelect","cascaderSelect","cascaderSelect.txt","temp","","views","CascaderSelect.vue"),
	businessRelationMapperResultMap("businessRelation","业务关联","mapperResultMap","mapperResultMap","mapperResultMap.txt","temp","mapperResultMap","mapperResultMap","MapperResultMap.txt"),
	baseCodeJavaNew("baseCode","基础代码新建java类","javaNew","javaNew","javaNew.txt","temp","javaNew","javaNew","JavaNew.txt"),
	baseCodeMapperResultMap("baseCode","基础代码基础mapperResultMap","mapperResultMap","mapperResultMap","mapperResultMap.txt","temp","mapperResultMap","mapperResultMap","MapperResultMap.txt");

	private String groupsValue;
	private String description;
	private String text;
	private String value;
	private String templatePath;
	private String type;
	private String packagePath;
	private String prefix;
	private String suffix;

	private TplEnum(String groupsValue,String description,String text,String value,String templatePath,String type,String packagePath,String prefix,String suffix){
		this.groupsValue=groupsValue;
		this.description=description;
		this.text=text;
		this.value=value;
		this.templatePath=this.groupsValue+ File.separator+templatePath;
		this.type=type;
		this.packagePath=packagePath;
		this.prefix=prefix;
		this.suffix=suffix;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

	public static TplEnum getTplEnum(String groupsValue,String value) {
		for (TplEnum tplEnum : TplEnum.values()) {
			if (groupsValue.equals(tplEnum.getGroupsValue())&&value.equals(tplEnum.getValue())) {
				return tplEnum;
			}
		}
		return null;
	}

	/*public static String generatorFilePath(TplEnum tplEnum, ProgrammeTable programmeTable, Programme programme){
		String result="";
		if("java".equals(tplEnum.getType())){
			result=programme.getJavaProjectPath()+File.separator+StringUtils.replace(programmeTable.getPackageName(), ".", File.separator)+File.separator;
			if(StringUtils.isNotBlank(tplEnum.getPackagePath())) {
				result += tplEnum.getPackagePath() + File.separator;
			}
			result += StringUtils.capitalize(programmeTable.getVarName())+tplEnum.getSuffix();
		}else if("vue".equals(tplEnum.getType())){
			result=programme.getFrontProjectPath()+File.separator+tplEnum.getPrefix()+File.separator+programmeTable.getVarName()+File.separator;
			if(StringUtils.isNotBlank(tplEnum.getPackagePath())) {
				result += tplEnum.getPackagePath() + File.separator;
			}
			result += programmeTable.getVarName()+tplEnum.getSuffix();
		}else if(Arrays.asList("temp","methodCode").contains(tplEnum.getType())){
			result=programme.getMethodCodePath()+File.separator+tplEnum.getType()+File.separator+tplEnum.getPrefix()+File.separator+programmeTable.getVarName()+File.separator;
			if(StringUtils.isNotBlank(tplEnum.getPackagePath())) {
				result += tplEnum.getPackagePath() + File.separator;
			}
			result += programmeTable.getVarName()+tplEnum.getSuffix();
		}
		return result;
	}*/

	public String getGroupsValue() {
		return groupsValue;
	}

	public void setGroupsValue(String groupsValue) {
		this.groupsValue = groupsValue;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getTemplatePath() {
		return templatePath;
	}

	public void setTemplatePath(String templatePath) {
		this.templatePath = templatePath;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getPackagePath() {
		return packagePath;
	}

	public void setPackagePath(String packagePath) {
		this.packagePath = packagePath;
	}

	public String getPrefix() {
		return prefix;
	}

	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}

	public String getSuffix() {
		return suffix;
	}

	public void setSuffix(String suffix) {
		this.suffix = suffix;
	}
}
