package org.springblade.modules.develop.beetl;

import java.io.File;
import java.io.FileOutputStream;
import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.beetl.core.GroupTemplate;
import org.beetl.core.Template;
import org.springblade.core.tool.utils.SpringUtil;
import org.springblade.modules.develop.service.IProgrammeService;
import org.springblade.modules.develop.utils.FileUtils;
import org.springframework.stereotype.Component;

@Component
public class BusinessTplGenerator extends GeneratorCode {

	public BusinessTplGenerator() {
	}

	public BusinessTplGenerator(String templatePath) {
		this.setTemplatePath(templatePath);
	}

	public BusinessTplGenerator(String templatePath, String generatorFilePath) {
		super(templatePath, generatorFilePath);
	}

	public String readTxt(){
		try{
			GroupTemplate gt = super.getGroupTemplate();
			Template t = gt.getTemplate(this.getTemplatePath());
			Map<String, Object> paras = getParas();
			t.fastBinding(paras);
			t.binding("programmeService", SpringUtil.getBean(IProgrammeService.class));
			/*FileUtils.createFile(this.getGeneratorFilePath());
			FileOutputStream out = new FileOutputStream(this.getGeneratorFilePath());
			t.renderTo(out);*/
			return t.render();
		}catch(Exception e){
			logger.error("出错：",e);
		}
		return "";
	}
}
