package org.springblade.modules.develop.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import org.springblade.core.mp.base.BaseServiceImpl;
import org.springblade.modules.develop.entity.ColumnInfo;
import org.springblade.modules.develop.entity.Datasource;
import org.springblade.modules.develop.entity.TableInfo;
import org.springblade.modules.develop.mapper.DatasourceMapper;
import org.springblade.modules.develop.mapper.MyBatisMapper;
import org.springblade.modules.develop.service.IMyBatisService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MyBatisServiceImpl extends BaseServiceImpl<MyBatisMapper, ColumnInfo> implements IMyBatisService {
	@Autowired
	private MyBatisMapper myBatisMapper;

	@Override
	public List<String> selectDatabases(){
		return myBatisMapper.selectDatabases();
	}

	@Override
	public IPage<TableInfo> selectTables(IPage page, String tableSchema, List<String> tableNamesList) {
		return page.setRecords(baseMapper.selectTables(tableSchema, tableNamesList,page));
	}

	@Override
	public ColumnInfo getColumnInfo(String tableSchema, String tableName, String columnName) {
		return myBatisMapper.getColumnInfo(tableSchema,tableName,columnName);
	}

	@Override
	public List<ColumnInfo> selectColumnInfo(String tableSchema, String tableName) {
		return myBatisMapper.selectColumnInfo(tableSchema, tableName);
	}
}
