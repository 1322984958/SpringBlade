package org.springblade.modules.develop.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import org.apache.ibatis.annotations.Param;
import org.springblade.core.mp.base.BaseService;
import org.springblade.modules.develop.entity.ColumnInfo;
import org.springblade.modules.develop.entity.Datasource;
import org.springblade.modules.develop.entity.TableInfo;

import java.util.List;

public interface IMyBatisService extends BaseService<ColumnInfo>{
	List<String> selectDatabases();

	IPage<TableInfo> selectTables(IPage page,String tableSchema, List<String> tableNamesList);

	ColumnInfo getColumnInfo(String tableSchema, String tableName, String columnName);

	List<ColumnInfo> selectColumnInfo(String tableSchema, String tableName);
}
