package org.springblade.modules.develop.dynamic;

import com.alibaba.druid.pool.DruidDataSource;
import com.baomidou.mybatisplus.extension.spring.MybatisSqlSessionFactoryBean;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.SqlSessionTemplate;
import org.mybatis.spring.annotation.MapperScan;
import org.springblade.core.boot.config.MybatisPlusConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;

@Configuration
//@MapperScan({"org.springblade.**.mapper.**"})
public class SqlSessionConfig{
	@Autowired
	private Environment env;

	@Bean("baseDataSource")
	public DataSource dataSource() {
		String driverClassName=env.getProperty("spring.datasource.driver-class-name");
		String url=env.getProperty("spring.datasource.url");
		String username=env.getProperty("spring.datasource.username");
		String password=env.getProperty("spring.datasource.password");
		DataSourceBuilder factory = DataSourceBuilder.create().driverClassName(driverClassName).url(url).username(username).password(password);
		return factory.build();
	}

	@Bean(name = "dynamicDataSource")
	//@ConditionalOnMissingBean
	//@Primary  // 优先使用，多数据源
	public DynamicDataSource dynamicDataSource(@Qualifier("baseDataSource") DataSource dataSource) {
		DynamicDataSource dynamicDataSource = new DynamicDataSource();
		Map<Object,Object> targetDataSources=new HashMap<>();
		targetDataSources.put("defaultDataSource",dataSource);
		dynamicDataSource.setTargetDataSources(targetDataSources);
		dynamicDataSource.setDefaultTargetDataSource(dataSource);
		return dynamicDataSource;
	}

	@Bean("sqlSessionFactory")
	public SqlSessionFactory sqlSessionFactory(DynamicDataSource dynamicDataSource) throws Exception {
		MybatisSqlSessionFactoryBean sqlSessionFactory = new MybatisSqlSessionFactoryBean();
		sqlSessionFactory.setDataSource(dynamicDataSource);
		return sqlSessionFactory.getObject();
	}

	@Bean // 事务管理
	public PlatformTransactionManager txManager(DynamicDataSource dynamicDataSource) {
		return new DataSourceTransactionManager(dynamicDataSource);
	}

	@Bean
	public JdbcTemplate getJdbcTemplate(DynamicDataSource dynamicDataSource){
		JdbcTemplate jdbcTemplate=new JdbcTemplate();
		jdbcTemplate.setDataSource(dynamicDataSource);
		return jdbcTemplate;
	}

	@Bean
	public SqlSessionTemplate sqlSessionTemplate(SqlSessionFactory sqlSessionFactory) throws Exception {
		return new SqlSessionTemplate(sqlSessionFactory);
	}
}
