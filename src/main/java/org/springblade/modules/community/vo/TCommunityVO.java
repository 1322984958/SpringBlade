package org.springblade.modules.community.vo;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springblade.modules.community.entity.TCommunity;

import java.util.Date;

@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "", description = "")
public class TCommunityVO extends TCommunity {
	private static final long serialVersionUID = 1L;
}
