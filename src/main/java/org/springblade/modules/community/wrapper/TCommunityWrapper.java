package org.springblade.modules.community.wrapper;

import org.springblade.core.mp.support.BaseEntityWrapper;
import org.springblade.core.tool.utils.BeanUtil;
import org.springblade.core.tool.utils.Func;
import org.springblade.core.tool.utils.SpringUtil;
import org.springblade.modules.community.entity.TCommunity;
import org.springblade.modules.community.vo.TCommunityVO;

import java.util.List;

/**
 * 包装类,返回视图层所需的字段
 */
public class TCommunityWrapper extends BaseEntityWrapper<TCommunity, TCommunityVO> {

	public static TCommunityWrapper build() {
		return new TCommunityWrapper();
	}

	@Override
	public TCommunityVO entityVO(TCommunity tCommunity) {
		TCommunityVO tCommunityVO = BeanUtil.copy(tCommunity, TCommunityVO.class);
		/*List<String> roleName = userService.getRoleName(user.getRoleId());
		List<String> deptName = userService.getDeptName(user.getDeptId());
		userVO.setRoleName(Func.join(roleName));
		userVO.setDeptName(Func.join(deptName));
		userVO.setSexName(dictService.getValue("sex", Func.toInt(user.getSex())));*/
		return tCommunityVO;
	}

}
