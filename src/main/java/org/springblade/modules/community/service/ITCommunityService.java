package org.springblade.modules.community.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.springblade.modules.community.entity.TCommunity;

public interface ITCommunityService extends IService<TCommunity> {
}
