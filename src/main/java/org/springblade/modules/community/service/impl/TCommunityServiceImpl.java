package org.springblade.modules.community.service.impl;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springblade.modules.community.entity.TCommunity;
import org.springblade.modules.community.mapper.TCommunityMapper;
import org.springblade.modules.community.service.ITCommunityService;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.core.metadata.IPage;

/**
 * service 实现类
 */
@Service
public class TCommunityServiceImpl extends ServiceImpl<TCommunityMapper, TCommunity> implements ITCommunityService {
}
