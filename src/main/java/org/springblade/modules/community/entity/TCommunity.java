package org.springblade.modules.community.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
@ApiModel(value = "小区表", description = "小区表")
public class TCommunity implements Serializable {
	private static final long serialVersionUID = 1L;

	/*****************/
	@ApiModelProperty(value = "")
	@TableId(value = "id", type = IdType.AUTO)
	private Long id;
	/**********小区名*******/
	@ApiModelProperty(value = "小区名")
	private String name;
	/**********省*******/
	@ApiModelProperty(value = "省")
	private String pro;
	/**********市*******/
	@ApiModelProperty(value = "市")
	private String city;
	/**********区*******/
	@ApiModelProperty(value = "区")
	private String area;
	/**********详细地址*******/
	@ApiModelProperty(value = "详细地址")
	private String addr;
	/**********经度*******/
	@ApiModelProperty(value = "经度")
	private String lng;
	/**********纬度*******/
	@ApiModelProperty(value = "纬度")
	private String lat;
	/**********小区面积*******/
	@ApiModelProperty(value = "小区面积")
	private String communityArea;
	/**********管理员id*******/
	@ApiModelProperty(value = "管理员id")
	private Long userId;
	/**********创建人*******/
	@ApiModelProperty(value = "创建人")
	private Long createUser;
	/**********创建时间*******/
	@ApiModelProperty(value = "创建时间")
	private Date createTime;
	/**********备注*******/
	@ApiModelProperty(value = "备注")
	private String remark;
}
