package org.springblade.modules.community.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
@ApiModel(value = "", description = "")
public class TCommunityAccountRecord implements Serializable {
	private static final long serialVersionUID = 1L;

	/*****************/
	@ApiModelProperty(value = "")
	@TableId(value = "id", type = IdType.AUTO)
	private Long id;
	/**********名称*******/
	@ApiModelProperty(value = "名称")
	private String name;
	/**********业主id*******/
	@ApiModelProperty(value = "业主id")
	private Long userId;
	/**********小区id*******/
	@ApiModelProperty(value = "小区id")
	private Long tCommunityId;
	/**********金额*******/
	@ApiModelProperty(value = "金额")
	private Double money;
	/**********分类： 1:收入,2:支出*******/
	@ApiModelProperty(value = "分类： 1:收入,2:支出")
	private Integer catagory;
	/**********类型:  1:物业费,2:车位管理费,3:水电费   等*******/
	@ApiModelProperty(value = "类型:  1:物业费,2:车位管理费,3:水电费   等")
	private Integer type;
	/**********缴费日期*******/
	@ApiModelProperty(value = "缴费日期")
	private Date time;
	/**********创建人*******/
	@ApiModelProperty(value = "创建人")
	private Long createUser;
	/**********创建时间*******/
	@ApiModelProperty(value = "创建时间")
	private Date createTime;
	/**********备注*******/
	@ApiModelProperty(value = "备注")
	private String remark;
}
