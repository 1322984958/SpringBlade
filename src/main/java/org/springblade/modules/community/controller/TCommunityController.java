package org.springblade.modules.community.controller;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springblade.modules.community.service.ITCommunityService;
import org.springblade.modules.community.vo.TCommunityVO;
import org.springblade.core.tool.api.R;
import org.springblade.modules.community.entity.TCommunity;
import org.springblade.modules.community.wrapper.TCommunityWrapper;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@AllArgsConstructor
@RequestMapping("/tCommunity")
@Api(value = "社区方法", tags = "社区方法")
public class TCommunityController{
	private ITCommunityService iTCommunityService;

	/**
	 * 社区方法
	 */
	@ApiOperationSupport(order = 1)
	@ApiOperation(value = "社区列表", notes = "社区列表")
	@GetMapping("/list")
	public R<IPage<TCommunityVO>> list(@Valid @RequestBody TCommunityVO tCommunityVO){
		return R.data(TCommunityWrapper.build().pageVO(new Page<TCommunity>()));
	}
}
