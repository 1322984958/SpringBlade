package org.springblade.modules.community.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.springblade.modules.community.entity.TCommunityAccountRecord;

/**
 * Mapper 接口
 */
public interface TCommunityAccountRecordMapper extends BaseMapper<TCommunityAccountRecord> {
}
