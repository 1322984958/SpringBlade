package org.springblade.modules.community.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springblade.modules.community.entity.TCommunity;

public interface TCommunityMapper extends BaseMapper<TCommunity> {

}
