<%
  var date = date();
  var programmeTable=@programmeTableService.getById(paramsJson['programmeTableId']);
  var tableProgrammeFieldList=@programmeTableFieldService.selectProgrammeTableFieldList(programmeTable.id, []);
%>
package ${programmeTable.packageName}.mapper;

import ${programmeTable.packageName}.entity.${sputil.capitalize(programmeTable.varName)};
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import java.util.List;

/**
 * ${programmeTable.name} Mapper 接口
 * @since ${date,dateFormat="yyyy-MM-dd HH:mm:ss"}  <>
 */
public interface ${sputil.capitalize(programmeTable.varName)}Mapper extends BaseMapper< ${sputil.capitalize(programmeTable.varName)}> {

}
