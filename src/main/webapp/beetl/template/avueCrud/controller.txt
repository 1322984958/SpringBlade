<%
  var date = date();
  var programmeTable=@programmeTableService.getById(paramsJson['programmeTableId']);
  var tableProgrammeFieldList=@programmeTableFieldService.selectProgrammeTableFieldList(programmeTable.id, []);
%>
package ${programmeTable.packageName}.controller;

import javax.validation.Valid;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.apache.commons.lang.StringUtils;
import org.springblade.core.boot.ctrl.BladeController;
import org.springblade.core.mp.support.Condition;
import org.springblade.core.mp.support.Query;
import org.springblade.core.tool.api.R;
import org.springblade.core.tool.utils.Func;
import ${programmeTable.packageName}.entity.${sputil.capitalize(programmeTable.varName)};
import ${programmeTable.packageName}.service.${sputil.capitalize(programmeTable.varName)}Service;
import ${programmeTable.packageName}.vo.${sputil.capitalize(programmeTable.varName)}VO;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.baomidou.mybatisplus.core.metadata.IPage;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.AllArgsConstructor;

import java.util.List;
import java.util.Date;

/**
 * ${programmeTable.name} 控制器
 * @since ${date,dateFormat="yyyy-MM-dd HH:mm:ss"}
 */
@RestController
@AllArgsConstructor
@RequestMapping("/${programmeTable.varName}/")
@Api(value = "${programmeTable.name}", tags = "${programmeTable.name}接口")
public class ${sputil.capitalize(programmeTable.varName)}Controller extends BladeController {
    private ${sputil.capitalize(programmeTable.varName)}Service ${programmeTable.varName}Service;
    private ${sputil.capitalize(programmeTable.varName)}Wrapper ${programmeTable.varName}Wrapper;

    /**
    * 详情
    */
    @GetMapping("/detail")
    @ApiOperation(value = "详情", notes = "详情", position = 1)
    public R<${sputil.capitalize(programmeTable.varName)}VO> detail(${sputil.capitalize(programmeTable.varName)} ${programmeTable.varName}) {
        ${sputil.capitalize(programmeTable.varName)} detail = ${programmeTable.varName}Service.getOne(Condition.getQueryWrapper(${programmeTable.varName}));
        return R.data(${programmeTable.varName}Wrapper.entityVO(detail));
    }

    /**
    * 分页 ${programmeTable.name}
    */
    @GetMapping("/list")
    @ApiOperation(value = "分页", notes = "分页", position = 2)
    public R<IPage<${sputil.capitalize(programmeTable.varName)}VO>> list(${sputil.capitalize(programmeTable.varName)} ${programmeTable.varName}, Query query) {
        QueryWrapper<${sputil.capitalize(programmeTable.varName)}> queryWrapper = new QueryWrapper<>();
        //queryWrapper.lambda().like(Programme::getName, o);
        IPage<${sputil.capitalize(programmeTable.varName)}> pages = ${programmeTable.varName}Service.page(Condition.getPage(query), queryWrapper);
        return R.data(${programmeTable.varName}Wrapper.pageVO(pages));
    }

    /**
    * 新增 ${programmeTable.name}
    */
    @PostMapping("/save")
    @ApiOperation(value = "新增", notes = "新增", position = 4)
    public R save(@Valid @RequestBody ${sputil.capitalize(programmeTable.varName)} ${programmeTable.varName}) {
        return R.status(${programmeTable.varName}Service.save(${programmeTable.varName}));
    }

    /**
    * 修改 ${programmeTable.name}
    */
    @PostMapping("/update")
    @ApiOperation(value = "修改", notes = "修改", position = 5)
    public R update(@Valid @RequestBody ${sputil.capitalize(programmeTable.varName)} ${programmeTable.varName}) {
        return R.status(${programmeTable.varName}Service.updateById(${programmeTable.varName}));
    }

    /**
    * 删除 ${programmeTable.name}
    */
    @PostMapping("/remove")
    @ApiOperation(value = "删除", notes = "传入ids删除", position = 7)
    public R remove(@ApiParam(value = "主键集合", required = true) @RequestParam String ids) {
        return R.status(${programmeTable.varName}Service.removeByIds(Func.toIntList(ids)));
    }

    @GetMapping("/selectAll")
    @ApiOperation(value = "查询全部", notes = "查询全部", position = 8)
    public R<List<${sputil.capitalize(programmeTable.varName)}VO>> selectAll() {
        List<${sputil.capitalize(programmeTable.varName)}> list = ${programmeTable.varName}Service.list();
        return R.data(${programmeTable.varName}Wrapper.listVO(list));
    }
}
