package ${packageName}.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.core.metadata.IPage;
<% if(isNotEmpty(importList)){%>
<%for(t in importList){%>
import ${t.packageName}.${t.className};
<%}%>
<%}%>

/**
 * service 实现类
 */
@Service
public class ${className}ServiceImpl extends ServiceImpl<${className}Mapper, ${className}> implements I${className}Service{
    /**
     * ${methodNote}
     */
    public ${returnClass.className}<% if(isNotEmpty(returnClass.fxClassName)){%><<%}%>${returnClass.fxClassName}<% if(isNotEmpty(returnClass.fxClassName)){%>><%}%> ${methodName}(
     <% if(isNotEmpty(paramsList)){%>
     <%for(t in paramsList){%>
     ${t.className} ${sputil.uncapitalize(t.className)}
     <% if(!tLP.first){%>,<%}%>
     <%}%>
     <%}%>
     );
}
