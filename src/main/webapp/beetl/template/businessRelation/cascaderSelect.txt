<%
  var date = date();
  var programmeTable=@programmeTableService.getById(paramsJson['programmeTableId']);
  var relationTableList=[];
  for(var i=0;i<paramsJson['relationTableId'].~size;i++){
    array.add(relationTableList,@programmeTableService.getById(paramsJson['relationTableId'][i]));
  }
%>
/******************************api********************************************/
<%for(t in relationTableList){%>
import { selectAll as ${t.varName}SelectAll} from '@/api/${t.varName}/${t.varName}'
<%}%>
/******************************api********************************************/

        /******************************crud********************************************/
        <%for(t in relationTableList){%>
        <template slot="${t.varName}Id" slot-scope="scope" >
          <el-tag>{{scope.row.${t.varName}VO.name}}</el-tag>
        </template>
        <%}%>
        /******************************crud********************************************/

        /******************************data********************************************/
        column: [
            {
              label: "${programmeTable.name}",
              prop: "${programmeTable.varName}Id",
			  type: "select",
			  dicUrl: "/api/${programmeTable.varName}/selectAll",
              props: {
                label: "name",
                value: "id"
              },
			  search: true,
			  slot:true,
			  width: '180px'
            },
            <%for(t in relationTableList){%>
            {
              label: "${t.name}",
              prop: "${t.varName}Id",
              type: "select",
              dicUrl: "/api/${t.varName}/selectAll",
              props: {
                label: "name",
                value: "id"
              },
              search: true,
              slot:true,
              width: '180px'
            },
            <%}%>
        ]
        /******************************data********************************************/


        /******************************watch********************************************/
        <%for(t in relationTableList){%>
        <%if(!tLP.last){%>
        'form.${t.varName}Id'() {
            this.option.column[this.$refs.crud.findColumnIndex("${relationTableList[tLP.index].varName}Id")].dicData=[]
            if (this.form.${t.varName}Id) {
              ${relationTableList[tLP.index].varName}SelectAll({'${t.varName}Id':this.form.${t.varName}Id}).then(res => {
                this.option.column[this.$refs.crud.findColumnIndex("${relationTableList[tLP.index].varName}Id")].dicData=res.data.data;
                this.form.${relationTableList[tLP.index].varName}Id='';
              });
            }
          },
          'searchForm.${t.varName}Id': function() {
            if (this.searchForm.${t.varName}Id) {
              ${relationTableList[tLP.index].varName}SelectAll({'${t.varName}Id':this.searchForm.${t.varName}Id}).then(res => {
                this.$refs.crud.DIC.${relationTableList[tLP.index].varName}Id = res.data.data;
                this.searchForm.${relationTableList[tLP.index].varName}Id=''
              });
            }
          },
          <%}%>
          <%}%>
        /******************************watch********************************************/
