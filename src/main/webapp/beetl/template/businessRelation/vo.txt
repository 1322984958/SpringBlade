<%
  var date = date();
  var programmeTable=@programmeTableService.getById(paramsJson['programmeTableId']);
  var relationTableList=[];
  for(var i=0;i<paramsJson['relationTableId'].~size;i++){
    array.add(relationTableList,@programmeTableService.getById(paramsJson['relationTableId'][i]));
  }
%>
package ${programmeTable.packageName}.vo;

import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import ${programmeTable.packageName}.entity.${sputil.capitalize(programmeTable.varName)};

/**
 * ${programmeTable.name} vo类
 * @since ${date,dateFormat="yyyy-MM-dd HH:mm:ss"}
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "${sputil.capitalize(programmeTable.varName)}对象", description = "${programmeTable.name}")
public class ${sputil.capitalize(programmeTable.varName)}VO extends ${sputil.capitalize(programmeTable.varName)}{
    <%for(t in relationTableList){%>
    private ${sputil.capitalize(t.varName)}VO ${t.varName}VO;
    <%}%>
}
